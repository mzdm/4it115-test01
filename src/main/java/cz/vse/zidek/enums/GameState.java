package cz.vse.zidek.enums;

/**
 * Výčtový typ, který obsahuje všechny možné stavy hry.
 *
 * <li>{@link #GAME_IS_RUNNING} - Hra stále běží</li>
 * <li>{@link #PLAYER_WON} - Hráč vyhrál</li>
 * <li>{@link #PLAYER_LOST_CAUGHT} - Hráč prohrál - byl přistižen</li>
 * <li>{@link #PLAYER_LOST_TIME_RAN_OUT} - Hráč prohrál - vypršel mu čas (tj. dosáhl max počet průchodů lokací)</li>
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public enum GameState {
    GAME_IS_RUNNING,
    PLAYER_WON,
    PLAYER_LOST_CAUGHT,
    PLAYER_LOST_TIME_RAN_OUT
}
