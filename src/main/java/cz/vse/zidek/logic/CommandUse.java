package cz.vse.zidek.logic;

import cz.vse.zidek.interfaces.ICommand;
import cz.vse.zidek.models.*;

/**
 * Třída implementující příkaz pro použití předmětu z inventáře.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandUse implements ICommand {
    private static final String NAME = "pouzit";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandUse(GamePlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí o použití předmětu z inventáře na nějaký objekt v aktuální lokaci, který
     * může vyvolat nějakou reakci / událost. Jsou itemy, které se po použití z
     * inventáře smažou, např. u piva nebo kusu oblečení.
     * Naopak u dalších se po použití předmět vrátí zpět do inventáře.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, která se vypíše na konzoli
     */
    @Override
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, co mám použít, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím použít více předmětů současně.";
        }

        String itemName = parameters[0];

        Area area = plan.getCurrentArea();
        Inventory inventory = plan.getInventory();

        if (!inventory.containsItem(itemName)) {
            return "Předmět '" + itemName + "' nemám u sebe.";
        }

        Item item = inventory.getItem(itemName);

        switch (itemName) {
            case "dalekohled":
                if (area.getName().equals("most")) {
                    String discoveredLocName = "melcina";
                    plan.getCurrentArea().getExitArea(discoveredLocName).setIsHidden(false);
                    return "Dalekohledem byla záhlednuta lokace '" + discoveredLocName + "'.";
                }
                break;
            /*
            case "rukavice":
                inventory.useItem(itemName);
                plan.incrementCurrentStealthStatus(1);
                return "Nasadil(a) sis rukavice, statistika plížení se zvýšila o 1.";
            case "pivo":
                inventory.useItem(itemName);
                plan.incrementCurrentStealthStatus(-1);
                return "Vypil(a) jsi pivo, statistika plížení se snížila o 1.";
            case "bunda":
                inventory.useItem(itemName);
                plan.incrementCurrentStealthStatus(2);
                return "Oblékl(a) sis bundu, statistika plížení se zvýšila o 2.";
            case "prasky":
                inventory.useItem(itemName);
                plan.incrementCurrentStealthStatus(-2);
                return "Polkl(a) jsi neznámé prášky, statistika plížení se snížila o 2.";
            case "uniforma":
                inventory.useItem(itemName);
                plan.incrementCurrentStealthStatus(6);
                return "Oblékl(a) sis vojenskou uniformu, statistika plížení se zvýšila o 6.";
                */
            case "batoh":
                inventory.useItem(itemName);
                plan.getInventory().incrementCapacity(2);
                return "Nasadil(a) sis batoh, kapacita inventáře se zvýšila o 2.";
            default:
                break;
        }

        inventory.useItem(itemName);
        plan.incrementCurrentStealthStatus(item.getStealthIncrease());
        return item.getUseStealthItemText();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}
