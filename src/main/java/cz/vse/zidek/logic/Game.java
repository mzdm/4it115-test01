package cz.vse.zidek.logic;

import cz.vse.zidek.enums.GameState;
import cz.vse.zidek.interfaces.ICommand;
import cz.vse.zidek.interfaces.IGame;
import cz.vse.zidek.models.ListOfCommands;

/**
 * Hlavní třída logiky aplikace. Třída vytváří instanci třídy {@link GamePlan},
 * která inicializuje lokace hry, a vytváří seznam platných příkazů a instance
 * tříd provádějících jednotlivé příkazy.
 * <p>
 * Během hry třída vypisuje uvítací a ukončovací texty a vyhodnocuje jednotlivé
 * příkazy zadané uživatelem.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Matěj Žídek
 * @version LS 2020
 */
public class Game implements IGame {
    private ListOfCommands listOfCommands;
    private GamePlan gamePlan;
    private boolean gameOver;

    /**
     * Konstruktor třídy. Vytvoří hru, inicializuje herní plán udržující
     * aktuální stav hry a seznam platných příkazů.
     */
    public Game() {
        gameOver = false;
        gamePlan = new GamePlan();
        listOfCommands = new ListOfCommands();

        listOfCommands.addCommand(new CommandHelp(listOfCommands));
        listOfCommands.addCommand(new CommandTerminate(this));
        listOfCommands.addCommand(new CommandMove(gamePlan));
        listOfCommands.addCommand(new CommandPick(gamePlan));
        listOfCommands.addCommand(new CommandInspect(gamePlan));
        listOfCommands.addCommand(new CommandInventory(gamePlan));
        listOfCommands.addCommand(new CommandDrop(gamePlan));
        listOfCommands.addCommand(new CommandUse(gamePlan));
        listOfCommands.addCommand(new CommandSpeak(gamePlan));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPrologue() {
        return "Vítejte!\n"
                + "Příběh je o útěku do atomového krytu.\n"
                + "Napište 'napoveda', pokud si nevíte rady, jak hrát dál.\n"
                + "\n"
                + gamePlan.getCurrentArea().getFullDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEpilogue() {
        String epilogue = "Díky, že sis zahrál(a) hru.";

        //System.out.println(">> TEST - Stav hry: " + gamePlan.getGameState());
        switch (gamePlan.getGameState()) {
            case PLAYER_WON:
                epilogue = "ZVÍTĚZIL(A) JSI !\n\n" + epilogue;
                break;
            case PLAYER_LOST_CAUGHT:
                epilogue = "BYL(A) JSI PŘISTIŽEN(A)!\nNÍZKÁ STATISTIKA PLÍŽENÍ\n\n" + epilogue;
                break;
            case PLAYER_LOST_TIME_RAN_OUT:
                epilogue = "NESTIHL(A) JSI SE UKRÝT !\n\n" + epilogue;
                break;
            default:
                break;
        }

        return epilogue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isGameOver() {
        return gameOver;
    }

    /**
     * Metoda nastaví příznak indikující, že nastal konec hry. Metodu
     * využívá třída {@link CommandTerminate}, mohou ji ale použít
     * i další implementace rozhraní {@link ICommand}.
     *
     * @param gameOver příznak indikující, zda hra již skončila
     */
    void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String processCommand(String line) {
        String[] words = line.split("[ \t]+");

        String cmdName = words[0];
        String[] cmdParameters = new String[words.length - 1];

        for (int i = 0; i < cmdParameters.length; i++) {
            cmdParameters[i] = words[i + 1];
        }

        String result = null;
        if (listOfCommands.checkCommand(cmdName)) {
            ICommand command = listOfCommands.getCommand(cmdName);
            result = command.process(cmdParameters);
        } else {
            result = "Nechápu, co po mně chceš. Tento příkaz neznám.";
        }

        if (gamePlan.isVictorious()) {
            gamePlan.setGameState(GameState.PLAYER_WON);
        }
        if (gamePlan.getGameState() == GameState.GAME_IS_RUNNING && gamePlan.isGameTimeOver()) {
            gamePlan.setGameState(GameState.PLAYER_LOST_TIME_RAN_OUT);
        }
        
        if (gamePlan.getGameState() != GameState.GAME_IS_RUNNING) {
            gameOver = true;
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GamePlan getGamePlan() {
        return gamePlan;
    }

}
