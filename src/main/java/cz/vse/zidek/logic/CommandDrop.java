package cz.vse.zidek.logic;

import cz.vse.zidek.models.Area;
import cz.vse.zidek.interfaces.ICommand;
import cz.vse.zidek.models.Inventory;

/**
 * Třída implementující příkaz pro položení předmětu z inventáře do lokace.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandDrop implements ICommand {
    private static final String NAME = "polozit";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandDrop(GamePlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí o položení zadaného předmětu do aktuální lokace. Aby tato
     * akce proběhla, musí zadaný předmět být v inventáři. Volné místo v lokaci se
     * neošetřuje, protože v lokaci může být neomezené množství předmětů.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, která se vypíše na konzoli
     */
    @Override
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, co mám položit, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím položit více předmětů současně.";
        }

        String itemName = parameters[0];

        Area area = plan.getCurrentArea();
        Inventory inventory = plan.getInventory();

        if (!inventory.containsItem(itemName)) {
            return "Předmět '" + itemName + "' nemám u sebe.";
        }

        area.addItem(inventory.dropItem(itemName));

        return "Položil(a) jsi předmět '" + itemName + "' a dal(a) jsi ho do současné lokace (" + area.getName() + ").";
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}
