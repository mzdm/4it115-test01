package cz.vse.zidek.logic;

import cz.vse.zidek.enums.GameState;
import cz.vse.zidek.interfaces.ICommand;
import cz.vse.zidek.models.*;

import java.util.*;

/**
 * Třída implementující příkaz pro promluvení s osobami v lokaci.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandSpeak implements ICommand {
    private static final String NAME = "mluvit";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandSpeak(GamePlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí o promluvení s jinými postavami. Některé postavy mohou
     * opakovat stále to samé, další naopak můženabídnout zajímavou směnu.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, která se vypíše na konzoli
     */
    @Override
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, s kým si mám promluvit, musíš zadat jméno osoby.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, nedokážu mluvit s více osobami zároveň.";
        }

        String personName = parameters[0];

        Area area = plan.getCurrentArea();
        Inventory inventory = plan.getInventory();

        if (!area.containsPerson(personName)) {
            return "Osoba '" + personName + "' se v této lokaci nenachází.";
        }

        Person person = area.getPerson(personName);

        if (person.getName().equals("kontrolor")) {
            if (inventory.containsItem("id_karta")) {
                plan.incrementCurrentStealthStatus(4);
            }

            if (plan.getCurrentStealthStatus() >= plan.getMinStealthRequired()) {
                if (area.getName().equals("brana")) {
                    String discoveredLocName = "atomovy_kryt";
                    plan.getCurrentArea().getExitArea(discoveredLocName).setIsHidden(false);
                    return person.getTalkEnd() + "\nKontrolol: (kývne)" + "\n\nByla zpřítupněna nová lokace '" + discoveredLocName + "'.";
                }
            } else {
                plan.setGameState(GameState.PLAYER_LOST_CAUGHT);
                return person.getTalkEnd() + "\nKontrolol: Prosím identifikujte se\nJá: (odporuji)\n* ZADRŽEN *";
            }
        }

        if (person.getDialogList() != null && !person.hasMoreTalk()) {
            Random random = new Random();
            int r = random.nextInt((person.getDialogListSize()));

            return person.getDialogList().get(r);
        }

        if (!person.hasMoreTalk()) {
            return person.getTalkEnd();
        }

        if (person.hasMoreTalk() && inventory.containsItem(person.getItemWanted())) {
            person.setHasMoreTalk(false);
            inventory.useItem(person.getItemWanted());

            Item myReceivedItem = area.getItem(person.getItemGiven());
            myReceivedItem.setIsHidden(false);

            String text = "Předmět '" + myReceivedItem.getName() + "' byl přidán do současné lokace (" + area.getName() + ") - inventář je plný.";
            if (inventory.addItem(myReceivedItem)) {
                area.removeItem(myReceivedItem.getName());
                text = "Předmět '" + myReceivedItem.getName() + "' přidán do inventáře.";
            }
            return person.getTalkMore() + "\nJá: Tady máš ten předmět\n" + person.getTalkEnd() + "\n" + text;
        }

        return person.getTalkMore();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}
