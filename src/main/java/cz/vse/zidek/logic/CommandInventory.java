package cz.vse.zidek.logic;

import cz.vse.zidek.interfaces.ICommand;
import cz.vse.zidek.models.Inventory;

/**
 * Třída implementující příkaz pro zobrazení informace o kapacitě inventáře
 * a aktuálně nesené předměty v inventáři.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandInventory implements ICommand {
    private static final String NAME = "inventar";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandInventory(GamePlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda vrací výpis stavu inventáře, tj. počet aktuálně nesených předmětů,
     * maximální kapacita inventáře a výpis všech předmětů v inventáři.
     *
     * @param parameters parametry příkazu <i>(aktuálně se ignorují)</i>
     * @return stav inventáře, který se vypíše na konzoli
     */
    @Override
    public String process(String... parameters) {
        Inventory inventory = plan.getInventory();

        if (inventory.getCurrentSize() == 0){
            return "Stav kapacity inventáře: (" + inventory.getCurrentSize() + "/" + inventory.getCapacity() + "). Momentálně neneseš žádné věci.";
        } 

        return "Stav kapacity inventáře: (" + inventory.getCurrentSize() + "/" + inventory.getCapacity() + "). Momentálně neseš tyto věci:" + inventory.getAllContentNames();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}
