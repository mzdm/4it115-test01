package cz.vse.zidek.logic;

import cz.vse.zidek.interfaces.ICommand;
import cz.vse.zidek.models.*;

/**
 * Třída implementující příkaz pro sebrání předmětu z lokace.
 *
 * @author Jan Říha
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandPick implements ICommand {
    private static final String NAME = "vzit";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandPick(GamePlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda se pokusí o sebrání předmětu z aktuální lokace a vložení do inventáře.
     * Aby se tato akce úspěšně provedla, zkoumá se, zda předmět v lokaci existuje, a
     * také zda kapacita inventáře není plná.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, která se vypíše na konzoli
     */
    @Override
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, co mám vzít, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím vzít více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (!area.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        }

        Item item = area.getItem(itemName);

        if (!item.isMovable()) {
            return "Předmět '" + itemName + "' fakt neunesu.";
        }

        Inventory inventory = plan.getInventory();

        if (!inventory.addItem(item)) {
            return "Tvůj inventář je plný, předmět '" + itemName + "' nebyl sebrán.";
        }

        area.removeItem(itemName);

        return "Vzal(a) jsi předmět '" + itemName + "' a uložil(a) jsi ho do inventáře.";
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }

}
