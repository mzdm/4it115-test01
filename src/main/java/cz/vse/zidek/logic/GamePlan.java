package cz.vse.zidek.logic;

import cz.vse.zidek.enums.GameState;
import cz.vse.zidek.models.Area;
import cz.vse.zidek.models.Inventory;
import cz.vse.zidek.models.Item;
import cz.vse.zidek.models.Person;

import java.util.*;

/**
 * Třída představující aktuální stav hry. Veškeré informace o stavu hry
 * <i>(mapa lokací, inventář, vlastnosti hlavní postavy, informace o postavách
 * apod.)</i> jsou uložené zde v podobě datových atributů.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Matěj Žídek
 * @version LS 2020
 */
public class GamePlan {
    private final int maxNumOfMoves, minStealthRequired;
    private final String finalLocationName;
    private GameState gameState = GameState.GAME_IS_RUNNING;
    private int currentNumOfMoves = 0, currentStealthStatus = 0;
    private Area currentArea;
    private Inventory inventory;

    /**
     * Konstruktor třídy. Pomocí metody {@link #prepareWorldMap() prepareWorldMap}
     * vytvoří jednotlivé lokace a propojí je pomocí východů.
     * <p>
     * K datovým atributům přiřadíme hodnoty
     */
    public GamePlan() {
        maxNumOfMoves = 45;
        minStealthRequired = 7;
        finalLocationName = "atomovy_kryt";
        inventory = new Inventory(3);
        prepareWorldMap();
    }

    /**
     * Metoda vytváří jednotlivé lokace a propojuje je pomocí východů. Jako
     * výchozí aktuální lokaci následně nastaví obývací pokoj, který se nachází
     * v domě postavy hráče.
     */
    private void prepareWorldMap() {
        // Vytvoříme jednotlivé lokace
        Area obyvaci_pokoj = new Area("obyvaci_pokoj", "Zde jsem často trávil svůj drahocenný volný čas. Z televize jdou slyšet varovné sirény, " +
                "které jdou i z venku. V rohu je lednice.");
        Area chodba = new Area("chodba", "Kolem mě visí různé druhy oblečení. Abych pronikl do krytu, musím vypadat co nejnenápadněji.");
        Area ulice = new Area("ulice", "Vždy rušná ulice je najednou liduprázdná. Kousek ode mě ale vidím jednoho pána.");
        Area obchod = new Area("obchod", "Teď už nikdo peníze přijímat nebude, přešlo se na směnný obchod. Třeba se mi něco podaří získat dobrého od obchodníka.");
        Area dum_souseda = new Area("dum_souseda", "Bylo odemčeno. Zřejmě se spěchali ukrýt.");
        Area garaz = new Area("garaz", "Často jsem si od nich půjčoval věci. Teďkom je tu ale strašný bordel.");
        Area most = new Area("most", "Je tu dobrý výhled na všechny strany.");
        Area brana = new Area("brana", "Naproti mě jsou kontroloři. Pokud za nimi zajdu a nebudu dostatečně vybaven, tak už nebude cesty zpět. Vidím, že řádně vybavení vojáci chodí skrz bez nějaké identifikace, pouze si kývnou.");
        Area melcina = new Area("melcina", "Řeka je znečištěná a mělčína je plná různých odpadků.", true);
        Area pole = new Area("pole", "Na poli je zaparkované vojenské auto bez dozoru.");
        Area atomovy_kryt = new Area(finalLocationName, "Dostal jsem se do krytu, teď už jsem v pohodě.", true);

        // Nastavíme průchody mezi lokacemi (sousední lokace)
        obyvaci_pokoj.addExit(chodba);
        chodba.addExit(obyvaci_pokoj);

        chodba.addExit(ulice);
        ulice.addExit(chodba);

        ulice.addExit(obchod);
        obchod.addExit(ulice);

        ulice.addExit(dum_souseda);
        dum_souseda.addExit(ulice);

        dum_souseda.addExit(garaz);
        garaz.addExit(dum_souseda);

        ulice.addExit(most);
        most.addExit(ulice);

        most.addExit(brana);
        brana.addExit(most);

        brana.addExit(atomovy_kryt);
        atomovy_kryt.addExit(brana);

        most.addExit(melcina);
        melcina.addExit(most);

        melcina.addExit(pole);
        pole.addExit(melcina);

        // Přidáme osoby do lokací
        ArrayList<String> dialogCivilista = new ArrayList<>();
        dialogCivilista.add("Civilista: (šeptá potichu) Mějte se na pozoru pane, doporučuji nosit co nejvíce krycího oblečení ať vás nepoznají při kontrole.");
        dialogCivilista.add("Civilista: (šeptá potichu) V domě naproti můžeš najít užitečné předměty.");
        dialogCivilista.add("Civilista: (šeptá potichu) Nic nevím.");
        
        Person civilista = new Person("civilista", dialogCivilista);
        Person obchodnik = new Person("obchodnik", true, "Obchodnik: Zdravím, mám pro tebe zajímavou nabídku. Když mi doneseš drona, dám ti vojenskou uniformu.",
                "Obchodník: Díky za směnný obchod, hodně štěstí.", "dron", "uniforma");
        Person kontrolor = new Person("kontrolor", "Já: (zadržuju dech)\nKontrolor: Podívá se na mně\nKontrolor: ...");

        ulice.addPerson(civilista);
        obchod.addPerson(obchodnik);
        brana.addPerson(kontrolor);

        // Přidáme předměty do lokací
        Item rukavice = new Item("rukavice", "Maskované rukavice, ty se můžou hodit.", false, true, 1, "Nasadil(a) sis rukavice, statistika plížení se zvýšila o 1.");
        Item lednice = new Item("lednice", "Rozměrná lednice, uvnitř ale téměř prázdná.", "pivo");
        Item pivo = new Item("pivo", "Chlazené pivo.", true, true, -1, "Vypil(a) jsi pivo, statistika plížení se snížila o 1.");
        Item televize = new Item("televize", "V televizi hlásají ať se co nejrychleji schováme.", false, false);

        Item bunda = new Item("bunda", "Kožená bunda.", false, true, 2, "Oblékl(a) sis bundu, statistika plížení se zvýšila o 2.");

        Item lampa = new Item("lampa", "Typická lampa.", false, false);
        Item kos = new Item("kos", "V odpadkovém koši asi nic užitečného není, hrabat se tam nebudu.", false, false);
        Item flaska = new Item("flaska", "Kus rozbité flašky.", false, true);

        Item noviny = new Item("noviny", "V těch novinách není nic co bych už nevěděl.", false, true);
        Item uniforma = new Item("uniforma", "Vojenská uniforma, včetně helmy a veškerého dalšího vybavení. S tímto mám vyhráno.", true, true, 6, "Oblékl(a) sis vojenskou uniformu, statistika plížení se zvýšila o 6.");

        Item batoh = new Item("batoh", "Celkem rozměrný batoh. Ten se hodí.", false, true);
        Item skrin = new Item("skrin", "Velká skříň, ve které ale nic užitečného není.", false, false);
        Item suplik = new Item("suplik", "Šuplík plný věcí.", "id_karta");
        Item id_karta = new Item("id_karta", "ID Karta souseda. Pracuje jako voják, snad není v problémech.", true, true);

        Item kupa_smeti = new Item("kupa_smeti", "Jen smetí.", false, false);
        Item kupa_hracek = new Item("kupa_hracek", "Spousta dětských hraček.", false, false);
        Item kupa_naradi = new Item("kupa_naradi", "Různé druhy nářadí.", "dalekohled");
        Item dalekohled = new Item("dalekohled", "Kvalitní dalekohled.", true, true);

        Item prasky = new Item("prasky", "V balení je už jen pár prášků.", false, true, -2, "Polkl(a) jsi neznámé prášky, statistika plížení se snížila o 2.");

        Item plechovka = new Item("plechovka", "Zrezavělá plechovka.", false, true);

        Item auto = new Item("auto", "Vojenské auto, které má otevřené dveře.", "dron");
        Item dron = new Item("dron", "Armádní dron. S drony neumím zacházet.", true, true);


        obyvaci_pokoj.addItem(rukavice);
        obyvaci_pokoj.addItem(lednice);
        obyvaci_pokoj.addItem(pivo);
        obyvaci_pokoj.addItem(televize);

        chodba.addItem(bunda);

        ulice.addItem(lampa);
        ulice.addItem(kos);
        ulice.addItem(flaska);

        obchod.addItem(noviny);
        obchod.addItem(uniforma);

        dum_souseda.addItem(batoh);
        dum_souseda.addItem(skrin);
        dum_souseda.addItem(suplik);
        dum_souseda.addItem(id_karta);

        garaz.addItem(dalekohled);
        garaz.addItem(kupa_smeti);
        garaz.addItem(kupa_hracek);
        garaz.addItem(kupa_naradi);

        most.addItem(prasky);

        melcina.addItem(plechovka);

        pole.addItem(auto);
        pole.addItem(dron);

        // Hru začneme v obývacím pokoji
        currentArea = obyvaci_pokoj;
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea() {
        return currentArea;
    }

    /**
     * Metoda nastaví aktuální lokaci, používá ji příkaz {@link CommandMove}
     * při přechodu mezi lokacemi.
     *
     * @param area lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area area) {
        currentArea = area;
    }

    /**
     * Metoda vrací jaký je aktuální stav hry.
     *
     * @return aktuální stav hry
     */
    public GameState getGameState() {
        return gameState;
    }

    /**
     * Metoda nastaví aktuální stav hry, který reprezentuje zda už není konec.
     *
     * @param gameState stav hry
     */
    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Metoda zkontroluje, zda hráč nedosáhl na maximální možný počet průchodů. V případě,
     * že dosáhne, tak hráč prohrává a hra končí.
     *
     * @return {@code true}, aktuální počet průchodů je rovno maximálnímu počtu průchodů; jinak {@code false}
     */
    public boolean isGameTimeOver() {
        return currentNumOfMoves == maxNumOfMoves;
    }

    /**
     * Metoda zkontroluje, zda se hráč nedostal do finální lokace. Pokud dostal, znamená to,
     * že se dostal do cíle a hru vyhrál.
     *
     * @return {@code true}, aktuální lokace je shodná s finální lokací; jinak {@code false}
     */
    public boolean isVictorious() {
        return finalLocationName.equals(currentArea.getName());
    }

    /**
     * Metoda zvyšuje aktuální hodnotu průchodů místností. Používá ji příkaz {@link CommandMove}.
     */
    public void incrementNumOfMoves() {
        currentNumOfMoves++;
    }

    /**
     * Metoda vrací číselnou hodnotu aktuálního počtu průchodů mezi lokacemi. To slouží
     * ke kontrole, zda-li hráč nedosáhl maximálního možného počtu průchodů - hra
     * by tím totiž skončila.
     *
     * @return aktuální počet průchodů
     */
    public int getCurrentNumOfMoves() {
        return currentNumOfMoves;
    }

    /**
     * Metoda vrací instanci hráčova inventáře.
     *
     * @return instance inventáře
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Metoda vrací aktuální číselnou hodnotu statistiky plížení. To slouží ke konci hry, kdy
     * se zkontroluje, zda-li hráč dosáhl minimálního požadovaného plížícího statu, jinak by
     * nebyl puštěn přes kontrolní bránu a došlo by k ukončení hry a prohry hráče.
     *
     * @return aktuální číselná hodnota statistiky plížení
     */
    public int getCurrentStealthStatus() {
        return currentStealthStatus;
    }

    /**
     * Metoda nastaví statistiku plížení na zadanou hodnotu.
     *
     * @param currentStealthStatus číselná hodnota statistiky plížení, na kterou chceme změnit z aktuální hodnoty
     */
    public void setCurrentStealthStatus(int currentStealthStatus) {
        this.currentStealthStatus = currentStealthStatus;
    }

    /**
     * Metoda přičte zadanou hodnotu k aktuální statistice plížení .
     * Pokud je zadaná hodnota záporná, dojde ke snížení aktuální hodnoty.
     *
     * @param value číselná hodnota, která se přičte k aktuální
     */
    public void incrementCurrentStealthStatus(int value) {
        this.currentStealthStatus += value;
    }

    /**
     * Metoda vrací hodnotu minimální požadované statistiky plížení. To slouží k ověření,
     * zda hráč tuto hranici dosáhl, a zda tak může pokračovat ve hře.
     *
     * @return číselná hodnota minimální požadované statistiky plížení
     */
    public int getMinStealthRequired() {
        return minStealthRequired;
    }
}
