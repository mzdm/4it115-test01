package cz.vse.zidek.logic;

import cz.vse.zidek.models.Area;
import cz.vse.zidek.interfaces.ICommand;
import cz.vse.zidek.models.Inventory;

/**
 * Třída implementující příkaz pro prozkoumání předmětu.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandInspect implements ICommand {
    private static final String NAME = "prozkoumat";

    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandInspect(GamePlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda vrací výpis popisu zadaného předmětu. Pokud tento předmět obsahuje další
     * předmět, tak ten předmět uvnitř se zviditelní a bude ho tak možné sebrat z lokace přes
     * {@code vzit}.
     *
     * @param parameters parametry příkazu <i>(ignoruje se nebo se očekává pole s jedním prvkem)</i>
     * @return informace pro hráče, která se vypíše na konzoli
     */
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return plan.getCurrentArea().getFullDescription();
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím prozkoumat více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        // Prozkoumávaný předmět může být v aktuální lokaci, ale i v inventáři
        Inventory inventory = plan.getInventory();

        // Podmínka, která kontroluje zda předmět vůbec je v lokaci, nebo v inventáři
        if (!area.containsItem(itemName) && !inventory.containsItem(itemName)) {
            return "Předmět '" + itemName + "' nemám u sebe a ani ho nikde nevidím.";
        }

        // Výpis popisu předmětu z inventáře
        if (inventory.containsItem(itemName)) {
           return inventory.getItem(itemName).getDescription(); 
        }

        // Výpis popisu předmětu z lokace, která obsahuje skrytý předmět, tento předmět se poté objeví v současné lokaci a u původního prozkoumaného předmětu se změní popis
        if (area.getItem(itemName).hasSearchableContent()) {
            area.getItem(area.getItem(itemName).getNameOfHiddenItem()).setIsHidden(false);
            area.getItem(itemName).setHasSearchableContent(false);
            return "V lokaci jsi nalezl(a) nový předmět '" + area.getItem(itemName).getNameOfHiddenItem() + "'!";
        }

        // Výpis popisu předmětu z lokace
        return area.getItem(itemName).getDescription();
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    public String getName() {
        return NAME;
    }
}
