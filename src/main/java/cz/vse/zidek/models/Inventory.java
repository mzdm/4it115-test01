package cz.vse.zidek.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Třída, která představuje inventář hráče. Nese informaci o kapacitě inventáře a jaké
 * předměty hráč má aktuálně v inventáři.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class Inventory {
    private int capacity;
    private Map<String, Item> items;

    /**
     * Konstruktor třídy inventář.
     *
     * @param maxCarrySize velikost inventáře (max počet předmětů)
     */
    public Inventory(int maxCarrySize) {
        this.capacity = maxCarrySize;
        items = new HashMap<>();
    }

    /**
     * Metoda vrací kolik předmětů je právě v inventáři.
     *
     * @return aktuální počet předmětů v inventáři
     */
    public int getCurrentSize() {
        return items.size();
    }

    /**
     * Metoda vrací kapacitu inventáře.
     *
     * @return kapacita inventáře
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Metoda vrací zda-li v inventáři je vyhledávaný předmět.
     *
     * @param itemName název hledaného předmětu
     * @return {@code true}, pokud se předmět nachází v inventáři; jinak {@code false}
     */
    public boolean containsItem(String itemName) {
        return items.containsKey(itemName);
    }

    /**
     * Metoda vrací všechny názvy předmětů, které jsou aktuálně v inventáři.
     *
     * @return textový řetězec obsahující názvy všech předmětů, které jsou v inventáři
     */
    public String getAllContentNames() {
        String values = "";
        for (String itemName : items.keySet()) {
            values += " " + itemName;
        }
        return values;
    }

    /**
     * Metoda pro přidání předmětu do inventáře. Aby se předmět přidal, musí mít inventář
     * volnou kapacitu.
     *
     * @param item předmět, který chceme přidat do inventáře
     * @return {@code true}, pokud se předmět přidal a vejde do inventáře; jinak {@code false}
     */
    public boolean addItem(Item item) {
        if (items.size() < capacity) {
            items.put(item.getName(), item);
            return true;
        }
        return false;
    }

    /**
     * Metoda, která použije zadaný předmět a poté ho odstraní z inventáře <i>(předmět si oblékneme, zkonzumujeme, apod.)</i>.
     *
     * @param itemName název předmětu, který chceme použít
     */
    public void useItem(String itemName) {
        items.remove(itemName);
    }

    /**
     * Metoda, která zvýší kapacitu inventáře o zadanou hodnotu.
     *
     * @param value číselná hodnota, o kterou chceme zvýšit aktuální maximální kapacitu inventáře
     */
    public void incrementCapacity(int value) {
        capacity += value;
    }

    /**
     * Metoda vrací předmět z inventáře zpět do aktuální lokace.
     *
     * @param itemName název předmětu, který chceme položit do lokace
     * @return pokládaný předmět
     */
    public Item dropItem(String itemName) {
        Item dropItem = items.get(itemName);
        items.remove(itemName);
        return dropItem;
    }

    /**
     * Metoda, která vrací instanci hledaného předmětu.
     *
     * @param itemName název hledaného předmětu
     * @return hledaný předmět
     */
    public Item getItem(String itemName) {
        return items.get(itemName);
    }
}
