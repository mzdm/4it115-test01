package cz.vse.zidek.models;

/**
 * Třída představující konkrétní předmět. Nese informaci o názvu, popisu,
 * viditelnosti a pohyblivosti předmětu. Předmět také může mít v sobě jiný předmět
 * <i>(např. v ledničce je pivo)</i>, v tom případě obsahuje i název obsaženého předmětu.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class Item {
    private String name;
    private String description;
    private boolean hidden;
    private boolean movable;

    private boolean hasSearchableContent;
    private String nameOfHiddenItem;
    
    private int stealthIncrease;
    private String useStealthItemText;

    public Item(String name, String description, boolean hidden, boolean movable, boolean hasSearchableContent, String nameOfHiddenItem) {
        this.name = name;
        this.description = description;
        this.hidden = hidden;
        this.movable = movable;
        this.hasSearchableContent = hasSearchableContent;
        this.nameOfHiddenItem = nameOfHiddenItem;
    }

    public Item(String name, String description, boolean hidden, boolean movable) {
        this(name, description, hidden, movable, false, "");
    }
    
    public Item(String name, String description, boolean hidden, boolean movable, int stealthIncrease, String useStealthItemText) {
        this(name, description, hidden, movable, false, "");
        this.stealthIncrease = stealthIncrease;
        this.useStealthItemText = useStealthItemText;
    }

    public Item(String name, String description, String nameOfHiddenItem) {
        this(name, description, false, false, true, nameOfHiddenItem);
    }
    
    public int getStealthIncrease() {
        return stealthIncrease;
    }
    
    public String getUseStealthItemText() {
        return useStealthItemText;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMovable() {
        return movable;
    }

    public void setMovable(boolean movable) {
        this.movable = movable;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setIsHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean hasSearchableContent() {
        return hasSearchableContent;
    }

    public void setHasSearchableContent(boolean hasSearchableContent) {
        this.hasSearchableContent = hasSearchableContent;
    }

    public String getNameOfHiddenItem() {
        return nameOfHiddenItem;
    }

    public void setNameOfHiddenItem(String nameOfHiddenItem) {
        this.nameOfHiddenItem = nameOfHiddenItem;
    }
}
