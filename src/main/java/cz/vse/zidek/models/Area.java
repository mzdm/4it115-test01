package cz.vse.zidek.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Třída představuje lokaci <i>(místo, místnost, prostor)</i> ve scénáři hry.
 * Každá lokace má název, který ji jednoznačně identifikuje. Lokace může mít
 * sousední lokace, do kterých z ní lze odejít. Odkazy na všechny sousední
 * lokace jsou uložené v kolekci.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Matěj Žídek
 * @version LS 2020
 */
public class Area {
    private String name;
    private String description;
    private boolean isHidden;
    private Set<Area> exits;  // Obsahuje sousední lokace, do kterých lze z této odejít
    private Map<String, Item> items;  // Obsahuje předměty v lokaci
    private Map<String, Person> people; // Obsahuje osoby v lokaci

    /**
     * Konstruktor třídy. Vytvoří lokaci se zadaným názvem, popisem a zda se jedná o
     * skrytou lokaci nebo ne.
     *
     * @param name        název lokace <i>(jednoznačný identifikátor, musí se jednat o text bez mezer)</i>
     * @param description podrobnější popis lokace
     * @param isHidden    zda-li je lokace skrytá nebo ne
     */
    public Area(String name, String description, boolean isHidden) {
        this.name = name;
        this.description = description;
        this.isHidden = isHidden;
        this.exits = new HashSet<>();
        this.items = new HashMap<>();
        this.people = new HashMap<>();
    }

    /**
     * Zkrácený konstruktor třídy. Vytvoří lokaci se zadaným názvem a popisem. Lokace nebude skrytá.
     *
     * @param name        název lokace <i>(jednoznačný identifikátor, musí se jednat o text bez mezer)</i>
     * @param description podrobnější popis lokace
     */
    public Area(String name, String description) {
        this(name, description, false);
    }

    /**
     * Metoda vrací název lokace, který byl zadán při vytváření instance jako
     * parametr konstruktoru. Jedná se o jednoznačný identifikátor lokace <i>(ve
     * hře nemůže existovat více lokací se stejným názvem)</i>. Aby hra správně
     * fungovala, název lokace nesmí obsahovat mezery, v případě potřeby můžete
     * více slov oddělit pomlčkami, použít camel-case apod.
     *
     * @return název lokace
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda přidá další východ z této lokace do lokace předané v parametru.
     * <p>
     * Vzhledem k tomu, že pro uložení sousedních lokací se používá {@linkplain Set},
     * může být přidán pouze jeden východ do každé lokace <i>(tzn. nelze mít dvoje
     * 'dveře' do stejné sousední lokace)</i>. Druhé volání metody se stejnou lokací
     * proto nebude mít žádný efekt.
     * <p>
     * Lze zadat též cestu do sebe sama.
     *
     * @param area lokace, do které bude vytvořen východ z aktuální lokace
     */
    public void addExit(Area area) {
        exits.add(area);
    }

    /**
     * Metoda vrací detailní informace o lokaci. Výsledek volání obsahuje název
     * lokace, podrobnější popis, seznam předmětů, seznam osob a seznam sousedních lokací, do kterých lze
     * odejít.
     *
     * @return detailní informace o lokaci
     */
    public String getFullDescription() {
        String exitNames = "Východy:";
        for (Area exitArea : exits) {
            if (!exitArea.isHidden) exitNames += " " + exitArea.getName();
        }

        String personNames = "Osoby:";
        for (String personName : people.keySet()) {
            personNames += " " + personName;
        }

        String itemNames = "Předměty:";
        for (String itemName : items.keySet()) {
            if (!items.get(itemName).isHidden()) itemNames += " " + itemName;
        }

        return "Jsi v lokaci (místnosti) " + name + ".\n"
                + description + "\n\n"
                + exitNames + "\n"
                + personNames + "\n"
                + itemNames;
    }

    /**
     * Metoda vrací lokaci, která sousedí s aktuální lokací a jejíž název
     * je předán v parametru. Pokud lokace s daným jménem nesousedí s aktuální
     * lokací, vrací se hodnota {@code null}.
     * <p>
     * Metoda je implementována pomocí tzv. 'lambda výrazu'. Pokud bychom chtěli
     * metodu implementovat klasickým způsobem, kód by mohl vypadat např. tímto
     * způsobem:
     * <pre> for (Area exitArea : exits) {
     *     if (exitArea.getName().equals(areaName)) {
     *          return exitArea;
     *     }
     * }
     *
     * return null;</pre>
     *
     * @param areaName jméno sousední lokace <i>(východu)</i>
     * @return lokace, která se nachází za příslušným východem; {@code null}, pokud aktuální lokace s touto nesousedí
     */
    public Area getExitArea(String areaName) {
        return exits.stream()
                .filter(exit -> exit.getName().equals(areaName))
                .findAny().orElse(null);
    }

    /**
     * Metoda přidá předmět <i>(objekt třídy {@link Item})</i> do lokace.
     *
     * @param item předmět, který bude do lokace přidán
     */
    public void addItem(Item item) {
        items.put(item.getName(), item);
    }

    /**
     * Metoda zkontroluje, zda je lokace skrytá.
     *
     * @return {@code true}, pokud je skrytá; jinak {@code false}
     */
    public boolean isHidden() {
        return isHidden;
    }

    /**
     * Metoda nastaví viditelnost lokace.
     *
     * @param newState značí, zda je lokace je skrytá
     */
    public void setIsHidden(boolean newState) {
        isHidden = newState;
    }

    /**
     * Metoda odebere předmět z lokace.
     *
     * @param itemName název odebíraného předmětu
     * @return odebraný předmět
     */
    public Item removeItem(String itemName) {
        return items.remove(itemName);
    }

    /**
     * Metoda vrací podle jména předmětu předmět z lokace.
     *
     * @param itemName název hledaného předmětu v lokaci
     * @return hledaný předmět
     */
    public Item getItem(String itemName) {
        return items.get(itemName);
    }

    /**
     * Metoda zkontroluje, zda-li se v lokaci nachází hledaný předmět. Pokud je hodnota
     * viditelnosti hledaného předmětu <i>skrytý</i>, tak i tak se předmět
     * v lokaci nenachází, protože nevíme, kde přesně je.
     *
     * @param itemName název předmětu
     * @return {@code true}, pokud se hledaný předmět nachází v lokaci; jinak {@code false}
     */
    public boolean containsItem(String itemName) {
        return items.containsKey(itemName) && !items.get(itemName).isHidden();
    }

    /**
     * Metoda přidá osobu <i>(objekt třídy {@link Person})</i> do lokace.
     *
     * @param person osoba, která bude do lokace přidána
     */
    public void addPerson(Person person) {
        people.put(person.getName(), person);
    }

    /**
     * Metoda vrací podle jména osoby osobu z lokace.
     *
     * @param personName název hledané osoby v lokaci
     * @return hledaná osoba
     */
    public Person getPerson(String personName) {
        return people.get(personName);
    }

    /**
     * Metoda zkontroluje, zda-li se v lokaci nachází hledaná osoba.
     *
     * @param personName název osoby
     * @return {@code true}, pokud se hledaná osoba nachází v lokaci; jinak {@code false}
     */
    public boolean containsPerson(String personName) {
        return people.containsKey(personName);
    }

    /**
     * Metoda porovnává dvě lokace <i>(objekty)</i>. Lokace jsou shodné,
     * pokud mají stejný název <i>(atribut {@link #name})</i>. Tato metoda
     * je důležitá pro správné fungování seznamu východů do sousedních
     * lokací.
     * <p>
     * Podrobnější popis metody najdete v dokumentaci třídy {@linkplain Object}.
     *
     * @param o objekt, který bude porovnán s aktuálním
     * @return {@code true}, pokud mají obě lokace stejný název; jinak {@code false}
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object o) {
        // Ověříme, že parametr není null
        if (o == null) {
            return false;
        }

        // Ověříme, že se nejedná o stejnou instanci (objekt)
        if (this == o) {
            return true;
        }

        // Ověříme, že parametr je typu (objekt třídy) Area
        if (!(o instanceof Area)) {
            return false;
        }

        // Provedeme 'tvrdé' přetypování
        Area area = (Area) o;

        // Provedeme porovnání názvů, statická metoda equals z pomocné třídy
        // java.util.Objects porovná hodnoty obou parametrů a vrátí true pro
        // stejné názvy a i v případě, že jsou oba názvy null; jinak vrátí
        // false
        return Objects.equals(this.name, area.name);
    }

    /**
     * Metoda vrací číselný identifikátor instance, který se používá
     * pro optimalizaci ukládání v dynamických datových strukturách
     * <i>(např.&nbsp;{@linkplain HashSet})</i>. Při překrytí metody
     * {@link #equals(Object) equals} je vždy nutné překrýt i tuto
     * metodu.
     * <p>
     * Podrobnější popis pravidel pro implementaci metody najdete
     * v dokumentaci třídy {@linkplain Object}.
     *
     * @return číselný identifikátor instance
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
