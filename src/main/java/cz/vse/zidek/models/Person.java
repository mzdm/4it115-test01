package cz.vse.zidek.models;

import java.util.*;

/**
 * Třída, která představuje konkrétní osobu. Nese informaci o jménu, co řekne při zavolání příkazu
 * {@code mluvit}, zda-li směňuje předmět a za co ho smění, a dialog po po úspěšné směně.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class Person {

    private String name;
    private boolean hasMoreTalk;
    private String talkEnd;
    private String talkMore;
    private String itemWanted;
    private String itemGiven;
    private ArrayList<String> dialogList;

    public Person(String name, boolean hasMoreTalk, String talkMore, String talkEnd, String itemWanted, String itemGiven) {
        this.name = name;
        this.hasMoreTalk = hasMoreTalk;
        this.talkMore = talkMore;
        this.talkEnd = talkEnd;
        this.itemWanted = itemWanted;
        this.itemGiven = itemGiven;
    }

    public Person(String name, String talkEnd) {
        this(name, false, "", talkEnd, "", "");
    }
    
    public Person(String name, ArrayList<String> dialogList) {
        this(name, false, "", "", "", "");
        this.dialogList = dialogList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasMoreTalk() {
        return hasMoreTalk;
    }

    public void setHasMoreTalk(boolean hasMoreTalk) {
        this.hasMoreTalk = hasMoreTalk;
    }

    public String getTalkEnd() {
        return talkEnd;
    }

    public void setTalkEnd(String talkEnd) {
        this.talkEnd = talkEnd;
    }

    public String getTalkMore() {
        return talkMore;
    }

    public void setTalkMore(String talkMore) {
        this.talkMore = talkMore;
    }

    public String getItemWanted() {
        return itemWanted;
    }

    public void setItemWanted(String itemWanted) {
        this.itemWanted = itemWanted;
    }

    public String getItemGiven() {
        return itemGiven;
    }
    
    public int getDialogListSize() {
        return dialogList.size();
    }
    
    public ArrayList<String> getDialogList() {
        return dialogList;
    }

    public void setItemGiven(String itemGiven) {
        this.itemGiven = itemGiven;
    }
}
