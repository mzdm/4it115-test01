package cz.vse.zidek.logic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Testovací třída pro komplexní otestování třídy {@link CommandSpeak}.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandSpeakTest {
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testProcess() {
        assertEquals("Nevím, s kým si mám promluvit, musíš zadat jméno osoby.", game.processCommand("mluvit"));
        assertEquals("Tomu nerozumím, nedokážu mluvit s více osobami zároveň.", game.processCommand("mluvit pan tady"));

        game.processCommand("jit chodba");
        game.processCommand("jit ulice");

        String commandOutput = game.processCommand("mluvit civilista");
        String randomDialog1 = "Civilista: (šeptá potichu) Mějte se na pozoru pane, doporučuji nosit co nejvíce krycího oblečení ať vás nepoznají při kontrole.";
        String randomDialog2 = "Civilista: (šeptá potichu) V domě naproti můžeš najít užitečné předměty.";
        String randomDialog3 = "Civilista: (šeptá potichu) Nic nevím.";
        assertTrue(commandOutput.equals(randomDialog1) || commandOutput.equals(randomDialog2) || commandOutput.equals(randomDialog3));

        game.processCommand("jit chodba");
        game.processCommand("jit ulice");

        assertEquals("Nevím, s kým si mám promluvit, musíš zadat jméno osoby.", game.processCommand("mluvit"));
        assertEquals("Tomu nerozumím, nedokážu mluvit s více osobami zároveň.", game.processCommand("mluvit pan tady"));

        game.processCommand("jit obchod");
        assertEquals("Obchodnik: Zdravím, mám pro tebe zajímavou nabídku. Když mi doneseš drona, dám ti vojenskou uniformu.", game.processCommand("mluvit obchodnik"));
        assertEquals("Obchodnik: Zdravím, mám pro tebe zajímavou nabídku. Když mi doneseš drona, dám ti vojenskou uniformu.", game.processCommand("mluvit obchodnik"));

        game.processCommand("jit ulice");
        game.processCommand("jit dum_souseda");
        game.processCommand("jit garaz");
        game.processCommand("prozkoumat kupa_naradi");
        game.processCommand("vzit dalekohled");
        game.processCommand("jit dum_souseda");
        game.processCommand("jit ulice");
        game.processCommand("jit most");
        game.processCommand("pouzit dalekohled");
        game.processCommand("jit melcina");
        game.processCommand("jit pole");
        game.processCommand("prozkoumat auto");
        game.processCommand("vzit dron");
        game.processCommand("jit melcina");
        game.processCommand("jit most");
        game.processCommand("jit ulice");
        game.processCommand("jit obchod");

        assertTrue(game.processCommand("mluvit obchodnik").contains("Já: Tady máš ten předmět"));

        assertEquals("Nevím, s kým si mám promluvit, musíš zadat jméno osoby.", game.processCommand("mluvit"));
        assertEquals("Tomu nerozumím, nedokážu mluvit s více osobami zároveň.", game.processCommand("mluvit pan tady"));

        assertEquals("Obchodník: Díky za směnný obchod, hodně štěstí.", game.processCommand("mluvit obchodnik"));

        assertEquals("Nevím, s kým si mám promluvit, musíš zadat jméno osoby.", game.processCommand("mluvit"));
        assertEquals("Tomu nerozumím, nedokážu mluvit s více osobami zároveň.", game.processCommand("mluvit pan tady"));
    }
}
