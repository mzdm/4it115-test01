package cz.vse.zidek.logic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Testovací třída pro komplexní otestování třídy {@link CommandInventory}.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandInventoryTest {
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testProcess() {
        assertEquals("Stav kapacity inventáře: (0/3). Momentálně neneseš žádné věci.", game.processCommand("inventar"));
        game.processCommand("vzit rukavice");
        assertNotEquals("Stav kapacity inventáře: (0/3). Momentálně neneseš žádné věci.", game.processCommand("inventar"));
        assertEquals("Stav kapacity inventáře: (1/3). Momentálně neseš tyto věci: rukavice", game.processCommand("inventar"));
        game.processCommand("polozit rukavice");
        assertEquals("Stav kapacity inventáře: (0/3). Momentálně neneseš žádné věci.", game.processCommand("inventar"));
        game.processCommand("vzit rukavice");
        assertEquals("Stav kapacity inventáře: (1/3). Momentálně neseš tyto věci: rukavice", game.processCommand("inventar"));
        game.processCommand("prozkoumat lednice");
        game.processCommand("vzit pivo");
        assertEquals("Stav kapacity inventáře: (2/3). Momentálně neseš tyto věci: pivo rukavice", game.processCommand("inventar"));
        game.processCommand("jit chodba");
        game.processCommand("vzit bunda");
        assertEquals("Stav kapacity inventáře: (3/3). Momentálně neseš tyto věci: bunda pivo rukavice", game.processCommand("inventar"));
        game.processCommand("jit ulice");
        game.processCommand("vzit noviny");
        assertEquals("Stav kapacity inventáře: (3/3). Momentálně neseš tyto věci: bunda pivo rukavice", game.processCommand("inventar"));
    }
}
