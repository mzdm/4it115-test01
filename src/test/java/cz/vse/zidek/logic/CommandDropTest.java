package cz.vse.zidek.logic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování třídy {@link CommandDrop}.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandDropTest {
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testProcess() {
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 0);
        assertEquals("Nevím, co mám položit, musíš zadat název předmětu.", game.processCommand("polozit"));
        assertEquals("Tomu nerozumím, neumím položit více předmětů současně.", game.processCommand("polozit pivo rukavice"));

        game.processCommand("vzit rukavice");
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 1);

        game.processCommand("prozkoumat lednice");
        game.processCommand("vzit pivo");

        assertEquals("Tomu nerozumím, neumím položit více předmětů současně.", game.processCommand("polozit pivo rukavice"));
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 2);
        assertEquals("Předmět 'noviny' nemám u sebe.", game.processCommand("polozit noviny"));
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 2);

        assertEquals("Položil(a) jsi předmět 'pivo' a dal(a) jsi ho do současné lokace (obyvaci_pokoj).", game.processCommand("polozit pivo"));
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 1);

        game.processCommand("vzit pivo");
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 2);

        assertEquals("Položil(a) jsi předmět 'pivo' a dal(a) jsi ho do současné lokace (obyvaci_pokoj).", game.processCommand("polozit pivo"));
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 1);

        assertEquals("Položil(a) jsi předmět 'rukavice' a dal(a) jsi ho do současné lokace (obyvaci_pokoj).", game.processCommand("polozit rukavice"));
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 0);

        assertEquals("Nevím, co mám položit, musíš zadat název předmětu.", game.processCommand("polozit"));
        assertEquals("Tomu nerozumím, neumím položit více předmětů současně.", game.processCommand("polozit pivo rukavice"));
        assertTrue(game.getGamePlan().getInventory().getCurrentSize() == 0);
    }
}
