package cz.vse.zidek.logic;

import cz.vse.zidek.enums.GameState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Testovací třída pro komplexní otestování herního příběhu .
 *
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Matěj Žídek
 * @version LS 2020
 */
public class GameTest {
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testPlayerQuit() {
        assertEquals("obyvaci_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("konec");
        assertTrue(game.isGameOver());
    }

    @Test
    public void testPlayerWon() {
        assertEquals("obyvaci_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("vzit rukavice");
        game.processCommand("pouzit rukavice");
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        game.processCommand("jit ulice");
        game.processCommand("jit dum_souseda");
        game.processCommand("jit garaz");
        game.processCommand("prozkoumat kupa_naradi");
        game.processCommand("vzit dalekohled");
        game.processCommand("jit dum_souseda");
        game.processCommand("jit ulice");
        game.processCommand("jit most");

        game.processCommand("pouzit dalekohled");
        assertEquals("most", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit melcina");
        game.processCommand("jit pole");
        game.processCommand("prozkoumat auto");
        game.processCommand("vzit dron");
        game.processCommand("jit melcina");
        game.processCommand("jit most");
        game.processCommand("jit ulice");

        game.processCommand("jit obchod");
        assertEquals("obchod", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("mluvit obchodnik");
        assertEquals("obchod", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("pouzit uniforma");
        assertEquals("obchod", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        game.processCommand("jit most");

        game.processCommand("jit brana");
        game.processCommand("mluvit kontrolor");
        assertEquals("brana", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit atomovy_kryt");
        assertEquals("atomovy_kryt", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.PLAYER_WON);
        assertTrue(game.isGameOver());
    }

    @Test
    public void testPlayerLostCaught() {
        assertEquals("obyvaci_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit most");
        assertEquals("most", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit brana");
        assertEquals("brana", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("mluvit kontrolor");
        assertEquals("brana", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.PLAYER_LOST_CAUGHT);
        assertTrue(game.isGameOver());

        assertNotEquals("most", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertTrue(game.isGameOver());
    }

    @Test
    public void testPlayerLostTimeRanOut() {
        assertEquals("obyvaci_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getCurrentNumOfMoves() == 0);
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getCurrentNumOfMoves() < 45);
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getCurrentNumOfMoves() < 45);
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getCurrentNumOfMoves() < 45);
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getCurrentNumOfMoves() < 45);
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit ulice");
        assertEquals("ulice", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertFalse(game.isGameOver());

        game.processCommand("jit chodba");
        assertEquals("chodba", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.getGamePlan().getCurrentNumOfMoves() == 45); // 45. průchod
        assertTrue(game.getGamePlan().getGameState() == GameState.PLAYER_LOST_TIME_RAN_OUT); // 45. průchod
        assertTrue(game.isGameOver());

        assertNotEquals("most", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.getGamePlan().getGameState() == GameState.GAME_IS_RUNNING);
        assertTrue(game.isGameOver());
    }
}
