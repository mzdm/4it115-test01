package cz.vse.zidek.logic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Testovací třída pro komplexní otestování třídy {@link CommandUse}.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class CommandUseTest {
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testProcess() {
        assertEquals("Nevím, co mám použít, musíš zadat název předmětu.", game.processCommand("pouzit"));
        assertEquals("Předmět 'paka' nemám u sebe.", game.processCommand("pouzit paka"));
        assertEquals("Tomu nerozumím, neumím použít více předmětů současně.", game.processCommand("pouzit toto tady"));

        game.processCommand("vzit rukavice");
        game.processCommand("prozkoumat lednice");
        game.processCommand("vzit pivo");

        assertEquals("Vypil(a) jsi pivo, statistika plížení se snížila o 1.", game.processCommand("pouzit pivo"));
        assertEquals("Nasadil(a) sis rukavice, statistika plížení se zvýšila o 1.", game.processCommand("pouzit rukavice"));

        game.processCommand("jit chodba");

        game.processCommand("vzit bunda");
        assertEquals("Oblékl(a) sis bundu, statistika plížení se zvýšila o 2.", game.processCommand("pouzit bunda"));

        game.processCommand("jit ulice");

        game.processCommand("jit most");
        game.processCommand("vzit prasky");
        assertEquals("Polkl(a) jsi neznámé prášky, statistika plížení se snížila o 2.", game.processCommand("pouzit prasky"));

        assertEquals("Nevím, co mám použít, musíš zadat název předmětu.", game.processCommand("pouzit"));
        assertEquals("Předmět 'paka' nemám u sebe.", game.processCommand("pouzit paka"));
        assertEquals("Tomu nerozumím, neumím použít více předmětů současně.", game.processCommand("pouzit toto tady"));

        game.processCommand("jit ulice");

        game.processCommand("jit dum_souseda");
        game.processCommand("vzit batoh");
        assertEquals("Nasadil(a) sis batoh, kapacita inventáře se zvýšila o 2.", game.processCommand("pouzit batoh"));

        game.processCommand("jit garaz");
        game.processCommand("prozkoumat kupa_naradi");
        game.processCommand("vzit dalekohled");
        game.processCommand("jit dum_souseda");
        game.processCommand("jit ulice");

        game.processCommand("jit most");
        assertEquals("Dalekohledem byla záhlednuta lokace 'melcina'.", game.processCommand("pouzit dalekohled"));

        game.processCommand("jit melcina");
        game.processCommand("jit pole");
        game.processCommand("prozkoumat auto");
        game.processCommand("vzit dron");
        game.processCommand("jit melcina");
        game.processCommand("jit most");
        game.processCommand("jit ulice");
        game.processCommand("jit obchod");
        game.processCommand("mluvit obchodnik");

        assertEquals("Oblékl(a) sis vojenskou uniformu, statistika plížení se zvýšila o 6.", game.processCommand("pouzit uniforma"));

        assertEquals("Nevím, co mám použít, musíš zadat název předmětu.", game.processCommand("pouzit"));
        assertEquals("Předmět 'paka' nemám u sebe.", game.processCommand("pouzit paka"));
        assertEquals("Tomu nerozumím, neumím použít více předmětů současně.", game.processCommand("pouzit toto tady"));
    }
}
