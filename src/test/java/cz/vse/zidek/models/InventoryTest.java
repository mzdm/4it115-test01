package cz.vse.zidek.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování třídy {@link Inventory}.
 *
 * @author Matěj Žídek
 * @version LS 2020
 */
public class InventoryTest {
    private Item item1, item2, item3, duplicate;
    private Inventory inventory;

    @Before
    public void setUp() {
        inventory = new Inventory(2);
        item1 = new Item("item1", "Toto je item1", false, false);
        item2 = new Item("item2", "Toto je item2", false, false);
        item3 = new Item("item3", "Toto je item3", false, false);
        duplicate = new Item("item3", "Toto je duplicate itemu3", false, false);
    }

    @Test
    public void getCurrentSizeTest() {
        assertTrue(inventory.getCurrentSize() == 0);

        inventory.addItem(item3);
        assertTrue(inventory.getCurrentSize() == 1);

        inventory.addItem(duplicate);
        assertTrue(inventory.getCurrentSize() == 1);

        inventory.addItem(item1);
        assertTrue(inventory.getCurrentSize() == 2);

        inventory.useItem(item3.getName());
        assertTrue(inventory.getCurrentSize() == 1);
    }

    @Test
    public void containsItemTest() {
        inventory.addItem(item3);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(item3.getName()));

        inventory.addItem(item1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.containsItem(item1.getName()));

        inventory.useItem(item3.getName());
        assertTrue(inventory.getCurrentSize() == 1);
        assertFalse(inventory.containsItem(item3.getName()));
    }

    @Test
    public void getAllContentNamesTest() {
        assertTrue(inventory.getCurrentSize() == 0);
        assertTrue(inventory.getAllContentNames().equals(""));


        inventory.addItem(item3);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(item3.getName()));
        assertTrue(inventory.getAllContentNames().equals(" item3"));

        inventory.addItem(duplicate);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(duplicate.getName()));
        assertTrue(inventory.getAllContentNames().equals(" item3"));


        inventory.addItem(item1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.containsItem(item1.getName()));
        assertTrue(inventory.getAllContentNames().equals(" item1 item3"));

        inventory.incrementCapacity(1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.getCapacity() == 3);

        inventory.addItem(item2);
        assertTrue(inventory.getCurrentSize() == 3);
        assertTrue(inventory.containsItem(item2.getName()));
        assertTrue(inventory.getAllContentNames().equals(" item2 item1 item3"));
    }

    @Test
    public void addItemTest() {
        inventory.addItem(item3);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(item3.getName()));

        inventory.addItem(duplicate);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(duplicate.getName()));

        inventory.addItem(item1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.containsItem(item1.getName()));

        inventory.useItem(item3.getName());
        assertTrue(inventory.getCurrentSize() == 1);
        assertFalse(inventory.containsItem(item3.getName()));
    }

    @Test
    public void useItemTest() {
        inventory.addItem(item3);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(item3.getName()));

        inventory.addItem(item1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.containsItem(item1.getName()));

        inventory.useItem(item3.getName());
        assertTrue(inventory.getCurrentSize() == 1);
        assertFalse(inventory.containsItem(item3.getName()));

        inventory.useItem(item1.getName());
        assertTrue(inventory.getCurrentSize() == 0);
        assertFalse(inventory.containsItem(item3.getName()));
    }

    @Test
    public void incrementCapacityTest() {
        inventory.addItem(item3);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(item3.getName()));

        inventory.addItem(duplicate);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(duplicate.getName()));

        inventory.addItem(item1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.containsItem(item1.getName()));

        inventory.incrementCapacity(1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.getCapacity() == 3);

        inventory.addItem(item2);
        assertTrue(inventory.getCurrentSize() == 3);
        assertTrue(inventory.containsItem(item2.getName()));
    }

    @Test
    public void dropItemTest() {
        inventory.addItem(item3);
        assertTrue(inventory.getCurrentSize() == 1);
        assertTrue(inventory.containsItem(item3.getName()));

        inventory.addItem(item1);
        assertTrue(inventory.getCurrentSize() == 2);
        assertTrue(inventory.containsItem(item1.getName()));

        inventory.dropItem(item3.getName());
        assertTrue(inventory.getCurrentSize() == 1);
        assertFalse(inventory.containsItem(item3.getName()));

        inventory.dropItem(item1.getName());
        assertTrue(inventory.getCurrentSize() == 0);
        assertFalse(inventory.containsItem(item3.getName()));
    }
}
